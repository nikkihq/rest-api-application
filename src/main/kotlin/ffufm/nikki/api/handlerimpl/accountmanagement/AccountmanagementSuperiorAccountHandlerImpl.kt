package ffufm.nikki.api.handlerimpl.accountmanagement

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.nikki.api.repositories.accountmanagement.AccountmanagementSuperiorAccountRepository
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccount
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccountDTO
import ffufm.nikki.api.spec.handler.accountmanagement.AccountmanagementSuperiorAccountDatabaseHandler
import kotlin.Long
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Component

@Component("accountmanagement.AccountmanagementSuperiorAccountHandler")
class AccountmanagementSuperiorAccountHandlerImpl :
        PassDatabaseHandler<AccountmanagementSuperiorAccount,
        AccountmanagementSuperiorAccountRepository>(),
        AccountmanagementSuperiorAccountDatabaseHandler {
    /**
     * Create SuperiorAccount: Creates a new SuperiorAccount object
     * HTTP Code 201: The created SuperiorAccount
     */
    override suspend fun create(body: AccountmanagementSuperiorAccountDTO): AccountmanagementSuperiorAccountDTO {
        val bodyEntity = body.toEntity()
        return repository.save(bodyEntity).toDto()
    }

    /**
     * Finds SuperiorAccounts by ID: Returns SuperiorAccounts based on ID
     * HTTP Code 200: The SuperiorAccount object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): AccountmanagementSuperiorAccountDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Delete SuperiorAccount by id.: Deletes one specific SuperiorAccount.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    /**
     * Update the SuperiorAccount: Updates an existing SuperiorAccount
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: AccountmanagementSuperiorAccountDTO, id: Long): AccountmanagementSuperiorAccountDTO {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original).toDto()
    }
}
