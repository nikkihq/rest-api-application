package ffufm.nikki.api.spec.dbo.accountmanagement

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Account for the superior user
 */
@Entity(name = "AccountmanagementSuperiorAccount")
@Table(name = "accountmanagement_superioraccount")
data class AccountmanagementSuperiorAccount(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Superior user&amp;amp;#039;s first name
     * Sample: Rommel
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "first_name"
    )
    val firstName: String = "",
    /**
     * Superior user&amp;amp;#039;s last name
     * Sample: Urbano
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "last_name"
    )
    @Lob
    val lastName: String = "",
    /**
     * Superior user&#039;s email address
     * Sample: rommel@rommel.com
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "email"
    )
    val email: String = "",
    /**
     * Superior user&#039;s type is set to Superior.
     * Sample: Superior
     */
    @ColumnDefault("'Superior'")
    @Column(
        length = 8,
        updatable = true,
        nullable = false,
        name = "user_type"
    )
    val userType: String = "Superior"
) : PassDTOModel<AccountmanagementSuperiorAccount, AccountmanagementSuperiorAccountDTO, Long>() {
    override fun toDto(): AccountmanagementSuperiorAccountDTO =
            super.toDtoInternal(AccountmanagementSuperiorAccountSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<AccountmanagementSuperiorAccount,
            AccountmanagementSuperiorAccountDTO, Long>, AccountmanagementSuperiorAccountDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()

    enum class Usertype(
        val value: String
    ) {
        SUPERIOR("Superior");
    }
}

/**
 * Account for the superior user
 */
data class AccountmanagementSuperiorAccountDTO(
    val id: Long? = null,
    /**
     * Superior user&amp;amp;#039;s first name
     * Sample: Rommel
     */
    val firstName: String? = "",
    /**
     * Superior user&amp;amp;#039;s last name
     * Sample: Urbano
     */
    val lastName: String? = "",
    /**
     * Superior user&#039;s email address
     * Sample: rommel@rommel.com
     */
    val email: String? = "",
    /**
     * Superior user&#039;s type is set to Superior.
     * Sample: Superior
     */
    val userType: String? = "Superior"
) : PassDTO<AccountmanagementSuperiorAccount, Long>() {
    override fun toEntity(): AccountmanagementSuperiorAccount =
            super.toEntityInternal(AccountmanagementSuperiorAccountSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<AccountmanagementSuperiorAccount,
            PassDTO<AccountmanagementSuperiorAccount, Long>, Long>,
            PassDTO<AccountmanagementSuperiorAccount, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class AccountmanagementSuperiorAccountSerializer :
        PassDtoSerializer<AccountmanagementSuperiorAccount, AccountmanagementSuperiorAccountDTO,
        Long>() {
    override fun toDto(entity: AccountmanagementSuperiorAccount):
            AccountmanagementSuperiorAccountDTO = cycle(entity) {
        AccountmanagementSuperiorAccountDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        email = entity.email,
        userType = entity.userType,

                )}

    override fun toEntity(dto: AccountmanagementSuperiorAccountDTO):
            AccountmanagementSuperiorAccount = AccountmanagementSuperiorAccount(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    email = dto.email ?: "",
    userType = dto.userType ?: "Superior",

            )
    override fun idDto(id: Long): AccountmanagementSuperiorAccountDTO =
            AccountmanagementSuperiorAccountDTO(
            id = id,
    firstName = null,
    lastName = null,
    email = null,
    userType = null,

            )}

@Service("accountmanagement.AccountmanagementSuperiorAccountValidator")
class AccountmanagementSuperiorAccountValidator :
        PassModelValidation<AccountmanagementSuperiorAccount> {
    override
            fun buildValidator(validatorBuilder: ValidatorBuilder<AccountmanagementSuperiorAccount>):
            ValidatorBuilder<AccountmanagementSuperiorAccount> = validatorBuilder.apply {
        konstraint(AccountmanagementSuperiorAccount::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(AccountmanagementSuperiorAccount::email) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(AccountmanagementSuperiorAccount::userType) {
            @Suppress("MagicNumber")
            lessThanOrEqual(8)
        }
    }
}

@Service("accountmanagement.AccountmanagementSuperiorAccountDTOValidator")
class AccountmanagementSuperiorAccountDTOValidator :
        PassModelValidation<AccountmanagementSuperiorAccountDTO> {
    override
            fun buildValidator(validatorBuilder: ValidatorBuilder<AccountmanagementSuperiorAccountDTO>):
            ValidatorBuilder<AccountmanagementSuperiorAccountDTO> = validatorBuilder.apply {
        konstraint(AccountmanagementSuperiorAccountDTO::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(AccountmanagementSuperiorAccountDTO::email) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(AccountmanagementSuperiorAccountDTO::userType) {
            @Suppress("MagicNumber")
            lessThanOrEqual(8)
        }
    }
}
