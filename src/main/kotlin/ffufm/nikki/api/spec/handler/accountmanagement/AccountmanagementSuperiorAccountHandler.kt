package ffufm.nikki.api.spec.handler.accountmanagement

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccountDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface AccountmanagementSuperiorAccountDatabaseHandler {
    /**
     * Create SuperiorAccount: Creates a new SuperiorAccount object
     * HTTP Code 201: The created SuperiorAccount
     */
    suspend fun create(body: AccountmanagementSuperiorAccountDTO):
            AccountmanagementSuperiorAccountDTO

    /**
     * Finds SuperiorAccounts by ID: Returns SuperiorAccounts based on ID
     * HTTP Code 200: The SuperiorAccount object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    suspend fun getById(id: Long): AccountmanagementSuperiorAccountDTO?

    /**
     * Delete SuperiorAccount by id.: Deletes one specific SuperiorAccount.
     */
    suspend fun remove(id: Long)

    /**
     * Update the SuperiorAccount: Updates an existing SuperiorAccount
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: AccountmanagementSuperiorAccountDTO, id: Long):
            AccountmanagementSuperiorAccountDTO
}

@Controller("accountmanagement.SuperiorAccount")
class AccountmanagementSuperiorAccountHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: AccountmanagementSuperiorAccountDatabaseHandler

    /**
     * Create SuperiorAccount: Creates a new SuperiorAccount object
     * HTTP Code 201: The created SuperiorAccount
     */
    @RequestMapping(value = ["/superioraccounts/"], method = [RequestMethod.POST])
    suspend fun create(@RequestBody body: AccountmanagementSuperiorAccountDTO): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body) }
    }

    /**
     * Finds SuperiorAccounts by ID: Returns SuperiorAccounts based on ID
     * HTTP Code 200: The SuperiorAccount object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    @RequestMapping(value = ["/superioraccounts/{id:\\d+}/"], method = [RequestMethod.GET])
    suspend fun getById(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.getById(id) ?: throw
                ResponseStatusException(HttpStatus.NOT_FOUND) }
    }

    /**
     * Delete SuperiorAccount by id.: Deletes one specific SuperiorAccount.
     */
    @RequestMapping(value = ["/superioraccounts/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the SuperiorAccount: Updates an existing SuperiorAccount
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/superioraccounts/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: AccountmanagementSuperiorAccountDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
