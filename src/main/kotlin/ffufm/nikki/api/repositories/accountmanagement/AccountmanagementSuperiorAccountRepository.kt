package ffufm.nikki.api.repositories.accountmanagement

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccount
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface AccountmanagementSuperiorAccountRepository : PassRepository<AccountmanagementSuperiorAccount, Long>
