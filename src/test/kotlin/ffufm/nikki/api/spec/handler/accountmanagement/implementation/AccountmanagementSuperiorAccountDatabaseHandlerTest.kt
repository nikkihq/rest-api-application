package ffufm.nikki.api.spec.handler.accountmanagement.implementation

import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.accountmanagement.AccountmanagementSuperiorAccountRepository
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccount
import ffufm.nikki.api.spec.handler.accountmanagement.AccountmanagementSuperiorAccountDatabaseHandler
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired

class AccountmanagementSuperiorAccountDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var accountmanagementSuperiorAccountDatabaseHandler:
            AccountmanagementSuperiorAccountDatabaseHandler


//    @Test
//    fun `test create`() = runBlocking {
//        val body: AccountmanagementSuperiorAccount = AccountmanagementSuperiorAccount()
//        accountmanagementSuperiorAccountDatabaseHandler.create(body)
//        Unit
//    }

    @Test
    fun `test getById`() = runBlocking {
        val id: Long = 0
        accountmanagementSuperiorAccountDatabaseHandler.getById(id)
        Unit
    }

    @Test
    fun `test remove`() = runBlocking {
        val id: Long = 0
        accountmanagementSuperiorAccountDatabaseHandler.remove(id)
        Unit
    }

//    @Test
//    fun `test update`() = runBlocking {
//        val body: AccountmanagementSuperiorAccount = AccountmanagementSuperiorAccount()
//        val id: Long = 0
//        accountmanagementSuperiorAccountDatabaseHandler.update(body, id)
//        Unit
//    }
}
