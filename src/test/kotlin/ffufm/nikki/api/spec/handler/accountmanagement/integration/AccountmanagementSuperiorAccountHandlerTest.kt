package ffufm.nikki.api.spec.handler.accountmanagement.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.nikki.api.PassTestBase
import ffufm.nikki.api.repositories.accountmanagement.AccountmanagementSuperiorAccountRepository
import ffufm.nikki.api.spec.dbo.accountmanagement.AccountmanagementSuperiorAccount
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class AccountmanagementSuperiorAccountHandlerTest : PassTestBase() {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `test create`() {
        val body: AccountmanagementSuperiorAccount = AccountmanagementSuperiorAccount()
                mockMvc.post("/superioraccounts/") {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test getById`() {
        val id: Long = 0
                mockMvc.get("/superioraccounts/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test remove`() {
        val id: Long = 0
                mockMvc.delete("/superioraccounts/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    
                }.andExpect {
                    status { isOk() }
                    
                }
    }

    @Test
    @WithMockUser
    fun `test update`() {
        val body: AccountmanagementSuperiorAccount = AccountmanagementSuperiorAccount()
        val id: Long = 0
                mockMvc.put("/superioraccounts/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                    
                }
    }
}
