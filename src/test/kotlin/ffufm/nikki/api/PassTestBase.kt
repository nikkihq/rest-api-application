package ffufm.nikki.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.nikki.api.repositories.accountmanagement.AccountmanagementSuperiorAccountRepository
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBNikki::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    lateinit var accountmanagementSuperiorAccountRepository:
            AccountmanagementSuperiorAccountRepository


    @Autowired
    lateinit var context: ApplicationContext

    @Before
    fun initializeContext() {
        SpringContext.context = context
    }

    @After
    fun cleanRepositories() {
        accountmanagementSuperiorAccountRepository.deleteAll()
    }
}
